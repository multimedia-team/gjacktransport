#!/bin/sh
echo "glib-gettextize .."
echo "no" | glib-gettextize --force --copy || exit

echo "intltoolize.."
intltoolize --copy --force --automake || exit
echo "aclocal.."
aclocal || exit
echo "autoheader.."
autoheader || exit
echo "automake.."
automake --gnu --add-missing --copy || exit
echo "autoconf.."
autoconf || exit

if test -z "$*"; then
  echo "**Warning**: I am going to run \`configure' with no arguments."
  echo "If you wish to pass any to it, please specify them on the"
  echo \`$0\'" command line."
  echo
fi

conf_flags="--enable-maintainer-mode"

echo Running $srcdir/configure $conf_flags "$@" ...
./configure $conf_flags "$@" \
&& echo Now type \`make\' to compile. || exit 1

