/* gjacktransport - jack transport slider
 *  Copyright (C) 2006, 2010 Robin Gareus  <robin AT gareus.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <math.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"
#include "gjack.h"
#include "gjacktransport.h"
#include "framerate.h"

// prototypes - defined in resourceconfig.c
void write_rc(const char *fn);
void try_load_rc_file (char *filename);

extern GtkWidget *window1;
extern gjtdata *stg; 
extern int mem;
extern int setup_done;
extern double stride;
extern FrameRate *fffr;
extern FrameRate *uffr;
extern FrameRate *jffr;
extern int j_have_vfps;
extern int max_mem;
extern int no_resize;
extern char *rc_file;
extern char *smpte_font;

int mode = 0;
gdouble z_pos = 50.0;
gdouble z_scale = 0.0;
gdouble fastskip = 0.0;

gdouble unit_table[UNIT_LAST] =  {
	3600.0, 60.0, 1.0, 1.0/48000.0, 
	1.0/10.0, 1.0/14.0, 1.0/15.0, 1.0/23.976, 1.0/24.0, 1.0/24.976,
	1.0/25.0, 1001.0/30000, 1.0/30.0, 1001.0/60.0 , 1.0/60 
};

#include <gdk/gdkkeysyms.h>
gint key_cfg[MAX_KEYBINDING] = { GDK_space, GDK_v, GDK_n, GDK_b, GDK_p, GDK_r, GDK_less, GDK_greater };
int key_update = -1;
int accept_num = 1;

const char key_names[MAX_KEYBINDING][12] = {
	"Play/Pause",
	"Fast Rew",
	"Fast FFw",
	"Play",
	"Pause",
	"Rewind",
	"Skip back",
	"Skip forward" 
};

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

int realloc_mem (int newmax) {
	if(newmax>99) return(-1);
	if(newmax<=max_mem) return(-2);
	int i;
	stg=realloc(stg, (newmax)*sizeof(gjtdata));
	for (i=max_mem; i< newmax; i++) {
		stg[i].unit=UNIT_SEC;
		stg[i].start=0.0;
		stg[i].end=60.0;
		stg[i].name=NULL;
	}
	max_mem=newmax;
	return 0;
}

void initialize_gjt (void) {
	int i;
	stg=calloc(max_mem, sizeof(gjtdata));
	for (i=0; i< max_mem; i++) {
		stg[i].unit=UNIT_SEC;
		stg[i].start=0.0;
		stg[i].end=60.0;
		stg[i].name=NULL;
	}
	stg[1].unit=UNIT_MIN;
	stg[1].end=3600.0;
	stg[2].unit=UNIT_HOUR;
	stg[2].end=2.5*3600.0;

	uffr = FR_create(25,1,FRF_NONE);
	jffr = FR_create(25,1,FRF_NONE);
	FR_setsamplerate(uffr,48000);
	FR_setsamplerate(jffr,48000);
	fffr = uffr; // pointer 
}

void showhide (char *togglebtn, char *widget, gboolean display) {
	GtkWidget *t=lookup_widget(window1,togglebtn);
	GtkWidget *w=lookup_widget(window1,widget);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(t),display);
	/* - is this [also] done by btn callback ? check */
	if (display) {
		gtk_widget_show (w);
	} else {
		gtk_widget_hide (w);
	}
}

gboolean ishidden (char *widget) {
	GtkWidget *w = lookup_widget(window1,widget);
	return (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(w)));
}

void dothejack (gboolean onoff) {
	GtkWidget *w = lookup_widget(window1,"checkbutton1");
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(w),onoff);
}

void select_mem (int newmem) {
	if (!setup_done) {
		mem=newmem;
		return;
	}
	if (newmem < 0 || newmem>= max_mem) return;
	mem=-1; //<  force on_combobox5_changed() to update the unit-comboboxes.
	GtkWidget *w = lookup_widget(window1,"combobox5");
	gtk_combo_box_set_active (GTK_COMBO_BOX(w),newmem);
	on_combobox5_changed(GTK_COMBO_BOX(w),NULL);
}

void update_startend (int newunit) {
	int oldunit=stg[mem].unit;
	gdouble val;
	gdouble min,max;
	stg[mem].unit=newunit; // NOTE: spinbtn callbacks use 'unit'
	if (newunit>3)
		FR_setdbl(uffr,1.0/unit_table[newunit],1);

	GtkWidget *spbtn1 = lookup_widget(window1,"spinbutton1");
	GtkWidget *spbtn2 = lookup_widget(window1,"spinbutton2");

	gtk_spin_button_get_range (GTK_SPIN_BUTTON(spbtn2), &min, &max);
	gtk_spin_button_set_range (GTK_SPIN_BUTTON(spbtn2), 0, max);

	val = gtk_spin_button_get_value (GTK_SPIN_BUTTON(spbtn2));
	val*=unit_table[oldunit]/unit_table[newunit];
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(spbtn2),val);

	val = gtk_spin_button_get_value (GTK_SPIN_BUTTON(spbtn1));
	val*=unit_table[oldunit]/unit_table[newunit];
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(spbtn1),val);

}

void update_combotxt (int pos) {
	/* TODO: check if entry exists before trying to remove ?!*/
	gtk_combo_box_remove_text(GTK_COMBO_BOX (lookup_widget(window1,"combobox5")), pos);
	if (stg[pos].name) {
		gtk_combo_box_insert_text(GTK_COMBO_BOX (lookup_widget(window1,"combobox5")), pos, stg[pos].name);
	} else {
		char name[4];
		if (pos<26) sprintf(name,"%c", 'A'+pos);
		else        sprintf(name,"%02i", pos-26);
		gtk_combo_box_insert_text(GTK_COMBO_BOX (lookup_widget(window1,"combobox5")), pos, name);
	}
}

void update_spinbtns (void) {
	GtkWidget *w;
	w = lookup_widget(window1,"spinbutton1");
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(w),stg[mem].start/unit_table[stg[mem].unit]);
	w = lookup_widget(window1,"spinbutton2");
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(w),stg[mem].end/unit_table[stg[mem].unit]);
}

void update_smpte_widget (GtkWidget *label_smpte, double sec) {
	gchar text[16];
#if 0
	gfloat secdiv = 100.0; 
	if (stg[mem].unit > 3) secdiv=1.0/unit_table[stg[mem].unit];
	if (sec < 0) snprintf(text, 16 , "--:--:--.--");
	else snprintf(text, 16 , "%02i:%02i:%02i.%02i",
		(int)floor(sec/3600.0),
		(int)floor((sec-3600.0*floor(sec/3600.0))/60.0),
		(int)floor(sec-60.0*floor(sec/60.0)),
		(int)floor(secdiv*(sec-floor(sec)))); // FIXME! rounding errors!
#else
	long int frame =  FR_af2vfi(fffr, sec * (double)fffr->samplerate);
	if (frame < 0) snprintf(text, 16 , "--:--:--.--");
	else FR_vf2smpte(fffr, text, frame);
#endif
	gtk_label_set_text(GTK_LABEL(label_smpte),text);
}

void update_startlabel (double sec) {
	GtkWidget *label_smpte = lookup_widget(window1,"label_start");
	update_smpte_widget(label_smpte,sec);
}

void update_endlabel (double sec) {
	GtkWidget *label_smpte = lookup_widget(window1,"label_end");
	update_smpte_widget(label_smpte,sec);
}

void update_smpte (double sec) {
	GtkWidget *label_smpte = lookup_widget(window1,"label_smpte");
	update_smpte_widget(label_smpte,sec);
}

gboolean limit_height (gpointer data) {
	GdkGeometry geo; 
	gtk_window_get_size (GTK_WINDOW(window1), &geo.max_width, &geo.max_height);
	geo.max_width=4096;
	gtk_window_set_geometry_hints(GTK_WINDOW (window1), NULL, &geo, GDK_HINT_MAX_SIZE);
	return FALSE;
}

void minimize_height (void) {
	gint width, height;
	if (no_resize) return;
	gtk_window_get_size (GTK_WINDOW(window1), &width, &height);
	height=40;
	gtk_window_resize(GTK_WINDOW(window1),width,height);
#if 0
	gtk_idle_add(limit_height, NULL);
#elif 0
	g_timeout_add(1,limit_height,NULL);
#else
	limit_height(NULL);
#endif
}

void post_rc_update (void) {
  int i;
  GtkWidget *w = lookup_widget(window1,"combobox5");
	GtkTreeModel *m=gtk_combo_box_get_model(GTK_COMBO_BOX(w));
	gint no=gtk_tree_model_iter_n_children(m,NULL);
  for (i=0;i<no; i++) {
		gtk_combo_box_remove_text(GTK_COMBO_BOX (w), i);
	}

  for (i=0;i<max_mem; i++) {
    update_combotxt(i);
  }

  if (mem >= max_mem) mem=max_mem-1;
  if (mem < 0) mem=0;
  //w = lookup_widget(window1,"combobox5"); // still used from above
  gtk_combo_box_set_active (GTK_COMBO_BOX(w),mem);
  update_spinbtns();
  if (smpte_font) 
    gtk_widget_modify_font (
	lookup_widget(window1,"label_smpte"), 
	pango_font_description_from_string (smpte_font));
}

void update_playpause_btn(void) {
	GtkWidget *w; 
	int val = jack_poll_transport_state();
	w = lookup_widget(window1,"button_pause");
	gtk_widget_set_sensitive(w,val&1);
	w = lookup_widget(window1,"button_play");
	gtk_widget_set_sensitive(w,val&2);
	w = lookup_widget(window1,"button_ff");
	gtk_widget_set_sensitive(w,val&4);
	w = lookup_widget(window1,"button_fr");
	gtk_widget_set_sensitive(w,val&4);
	w = lookup_widget(window1,"button_rewind");
	gtk_widget_set_sensitive(w,val&4);
	w = lookup_widget(window1,"button_end");
	gtk_widget_set_sensitive(w,val&4);
	w = lookup_widget(window1,"button_pos");
	gtk_widget_set_sensitive(w,val&4);
	w = lookup_widget(window1,"button6");
	gtk_widget_set_sensitive(w,val&4);
}

/* main-timeout function that moves the
   slider along with jack-transport */
gboolean gpolljack (gpointer data) {
	GtkWidget *scale = lookup_widget(window1,"hscale1");
	double val=500;
	double jt=jack_poll_time();
	update_playpause_btn();

	if (jt < 0 ) { dothejack(0); return (TRUE);}
	update_smpte(jt);

	val = (jt-stg[mem].start)/(stg[mem].end-stg[mem].start)*1000.0;

	if (mode != 0 ) { return (TRUE);}
	gtk_range_set_value (GTK_RANGE(scale),val);
	return(TRUE);
}

gboolean gfastskip (gpointer data) {
	if (fastskip == 0.0) {mode=0; return (FALSE); }

	GtkWidget *scale = lookup_widget(window1,"hscale1");
	mode=1;
	double val = gtk_range_get_value (GTK_RANGE(scale));
	gtk_range_set_value (GTK_RANGE(scale),val+fastskip);

#if 1
	if (fastskip < 100.0) 
		//fastskip+=(fastskip/5.0);
		fastskip*=1.1;
#endif
	if (val+fastskip < 0 || val+fastskip > 999.0) mode=0;
	return(TRUE);
}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

void set_dialog_smpte(GtkWidget *p, gdouble val) {
	GtkWidget *w = lookup_widget(p,"radiobuttonS_jack");
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(w),fffr==jffr);
	gtk_widget_set_sensitive(w,j_have_vfps); /// check jack tansport_t->valid 
	if (!j_have_vfps) fffr=uffr;
	w = lookup_widget(p,"radiobuttonS_vf");
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(w),fffr!=jffr);
	w = lookup_widget(p,"checkbuttonS_dftc");
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(w),(uffr->flags&FRF_DROP_FRAMES));

	w = lookup_widget(p,"entry1");
	if (stg[mem].name)
		gtk_entry_set_text(GTK_ENTRY(w), stg[mem].name);
	else 
		gtk_entry_set_text(GTK_ENTRY(w), 
				gtk_combo_box_get_active_text(GTK_COMBO_BOX(lookup_widget(window1,"combobox5"))));

	long int bcd[SMPTE_LAST];
	// FIXME: get vframe from spinbutton in parent function (don't multiply/round)
	//long int vframe = (long int) floor(val*(gdouble)fffr->num/(gdouble)fffr->den);
	long int vframe =  FR_af2vfi(fffr, val * (double)fffr->samplerate);
	FR_frame_to_bcd(fffr,bcd, vframe);
  	//printf("DEBUG %f -> %f\n", val, vframe);
		

	w = lookup_widget(p,"spinbuttonS_h");
	gtk_spin_button_set_value (GTK_SPIN_BUTTON(w),bcd[SMPTE_HOUR]);
	w = lookup_widget(p,"spinbuttonS_m");
	gtk_spin_button_set_value (GTK_SPIN_BUTTON(w),bcd[SMPTE_MIN]);
	w = lookup_widget(p,"spinbuttonS_s");
	gtk_spin_button_set_value (GTK_SPIN_BUTTON(w),bcd[SMPTE_SEC]);
	w = lookup_widget(p,"spinbuttonS_f");
	gtk_spin_button_set_range (GTK_SPIN_BUTTON(w), 0, FR_toint(fffr)-1); 
	gtk_spin_button_set_value (GTK_SPIN_BUTTON(w),bcd[SMPTE_FRAME]);
}

gdouble parse_dialog_smpte(GtkWidget *p) {
	GtkWidget *w = lookup_widget(p,"radiobuttonS_jack");
	if (j_have_vfps && gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(w))) 
		fffr=jffr;
	w = lookup_widget(p,"radiobuttonS_vf");
	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(w))) 
		fffr=uffr;
	w = lookup_widget(p,"checkbuttonS_dftc");
	uffr->flags= (fffr->flags&~FRF_DROP_FRAMES)|((gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(w)))?FRF_DROP_FRAMES:0);

	int  h,m,s,f;
	w = lookup_widget(p,"spinbuttonS_h");
	h = (int) gtk_spin_button_get_value (GTK_SPIN_BUTTON(w));
	w = lookup_widget(p,"spinbuttonS_m");
	m = (int) gtk_spin_button_get_value (GTK_SPIN_BUTTON(w));
	w = lookup_widget(p,"spinbuttonS_s");
	s = (int) gtk_spin_button_get_value (GTK_SPIN_BUTTON(w));
	w = lookup_widget(p,"spinbuttonS_f");
	f = (int) gtk_spin_button_get_value (GTK_SPIN_BUTTON(w));

	long int vframe = FR_smpte2vf(fffr,f,s,m,h,0);	
#if 1
	return ((gdouble)vframe*(gdouble)fffr->den/(gdouble)fffr->num);
#else 
	long long int aframe = FR_vf2af(fffr,vframe);
	// printf(" %02i:%02i:%02i.%02i = %li = %lli\n",h,m,s,f, vframe, aframe);
	return ((gdouble) aframe / (gdouble) fffr->samplerate ); 
#endif
}

void apply_smpte(gdouble *dst, gdouble val) {
	*dst=val;
	update_spinbtns();
}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

GtkWidget *key_dlg;

void prepare_key (int ptr) {
	char txt[64];
	GtkLabel *l = GTK_LABEL(lookup_widget(key_dlg,"label_status"));
	if (key_update!=-1) {
		snprintf(txt,64,"2nd button press - set '%s' cancelled.", key_names[key_update]);
		gtk_label_set_text(GTK_LABEL(l),txt);
		key_update=-1;
		return;
	}
	key_update=ptr;
	snprintf(txt,64,"press key to bind '%s' - or 'Esc' to unset.", key_names[key_update]);
	gtk_label_set_text(l,txt );
}

void set_key (int key) {
	if (  (key>=GDK_Shift_L && key<=GDK_Hyper_R)
			||(key>=GDK_ISO_Lock && key <= GDK_ISO_Level5_Lock)
		 ){
		/* ignore shift, CTRL, META, ... */
		return;
	}
	//printf("DEBUG key-code 0x%x = %i\n", key, key);

	GtkLabel *l = GTK_LABEL(lookup_widget(key_dlg,"label_status"));
	gtk_label_set_text(l,"-" );
	int i;
	char txt[64];
	char kxt[20];
	if (key_update<0 || key_update>= MAX_KEYBINDING ) return;
	for (i=0; i<MAX_KEYBINDING; i++) { 
		if(i==key_update) continue;
		if (key>=GDK_1 && key<=GDK_6) {
			gtk_label_set_text(GTK_LABEL(l),"keys 1-6 are not programmable." );
			key_update=-1;
			return;
		}
		if (key_cfg[i]==key && key_cfg[i]!=GDK_Escape) {
			gtk_label_set_text(GTK_LABEL(l),"This key is already in use." );
			key_update=-1;
			return;
		}
	}

	snprintf(txt,64,"labelK%i",key_update);
	GtkLabel *bl = GTK_LABEL(lookup_widget(key_dlg,txt));

	key_cfg[key_update]=key;
	if (key_cfg[key_update]==GDK_Escape) {
		snprintf(txt,64,"disabled keybinding: %s.\n",key_names[key_update]);
		snprintf(kxt,20,": [off]");
	} else if (key_cfg[key_update]==GDK_Tab) {
		snprintf(txt,64,"Key %s: [Tab]\n", key_names[key_update]);
		snprintf(kxt,20,": [Tab]");
	} else if (key_cfg[key_update]==GDK_BackSpace) {
		snprintf(txt,64,"Key %s: [BackSpace]\n", key_names[key_update]);
		snprintf(kxt,20,": [BackSpace]");
	} else if (key_cfg[key_update]==GDK_space) {
		snprintf(txt,64,"Key %s: [Space-bar]\n", key_names[key_update]);
		snprintf(kxt,20,": [Space-bar]");
	} else if (key_cfg[key_update]==GDK_Return) {
		snprintf(txt,64,"Key %s: [Enter]\n", key_names[key_update]);
		snprintf(kxt,20,": [Enter]");
	} else if (key_cfg[key_update]>0 && key_cfg[key_update]< 256) {
		snprintf(txt,64,"Key %s: '%c'\n", key_names[key_update],key_cfg[key_update]&0xff);
		snprintf(kxt,20,": '%c'", key_cfg[key_update]&0xff);
	} else {
		snprintf(txt,64,"Key %s: [non-printable-character]\n", key_names[key_update]);
		snprintf(kxt,20,": [non-print-char]");
	}
	key_update=-1;
	gtk_label_set_text(GTK_LABEL(l),txt);
	gtk_label_set_text(GTK_LABEL(bl),kxt);
}
/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

void
on_hscale1_value_changed               (GtkRange        *range,
                                        gpointer         user_data)
{
	gdouble val = gtk_range_get_value (range);
	gdouble sec = val*(stg[mem].end-stg[mem].start)/1000.0 + stg[mem].start;
	if (mode == 0 ) { return;}
	status_printf("jack reposition: %.3f sec", sec);
	jack_reposition(sec);
	if (mode == 2 ) {mode=0;}
}

void seek_relative(double sec_offset) {
	if (mode != 0 ) { return;}
	double val = jack_poll_time();
	if (val+sec_offset>0)
		jack_reposition(val+sec_offset);
	else
		jack_reposition(0.0);
}


void
on_checkbutton1_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
	GtkWidget *widget = lookup_widget(window1,"image15");
	if ( gtk_toggle_button_get_active(togglebutton)) {
		open_jack();
	} else {
		close_jack();
	}
	if (jack_connected()) {
		gtk_image_set_from_icon_name(GTK_IMAGE(widget),"gtk-connect",GTK_ICON_SIZE_BUTTON);
		gtk_toggle_button_set_active(togglebutton,TRUE);
	} else  {
		gtk_image_set_from_icon_name(GTK_IMAGE(widget),"gtk-disconnect",GTK_ICON_SIZE_BUTTON);
		gtk_toggle_button_set_active(togglebutton,FALSE);
	}

}


void
on_spinbutton1_value_changed           (GtkSpinButton   *spinbutton,
                                        gpointer         user_data)
{
	GtkWidget *spbtn1 = lookup_widget(window1,"spinbutton1");
	GtkWidget *spbtn2 = lookup_widget(window1,"spinbutton2");
	gdouble val = gtk_spin_button_get_value (GTK_SPIN_BUTTON(spbtn1));
	gdouble min,max;
	gtk_spin_button_get_range (GTK_SPIN_BUTTON(spbtn2), &min, &max);
	gtk_spin_button_set_range (GTK_SPIN_BUTTON(spbtn2), val+(1.0/unit_table[stg[mem].unit]), max);

	stg[mem].start=val * unit_table[stg[mem].unit];
	update_startlabel(stg[mem].start);
}


void
on_spinbutton2_value_changed           (GtkSpinButton   *spinbutton,
                                        gpointer         user_data)
{
	GtkWidget *spbtn2 = lookup_widget(window1,"spinbutton2");
	gdouble val = gtk_spin_button_get_value (GTK_SPIN_BUTTON(spbtn2));
	stg[mem].end=val * unit_table[stg[mem].unit];
	update_endlabel(stg[mem].end);
}


void
on_hscale1_move_slider                 (GtkRange        *range,
                                        GtkScrollType    scroll,
                                        gpointer         user_data)
{
	mode=2;
#if 0
	on_hscale1_value_changed(range,NULL);
#endif
}


gboolean
on_hscale1_button_press_event          (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
	mode=1;
	return FALSE;
}


gboolean
on_hscale1_button_release_event        (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
	mode=0;
	return FALSE;
}




gboolean
on_hscale1_change_value                (GtkRange        *range,
                                        GtkScrollType    scroll,
                                        gdouble          value,
                                        gpointer         user_data)
{
  return FALSE;
}


void
on_button_rewind_released              (GtkButton       *button,
                                        gpointer         user_data)
{
	GtkWidget *scale = lookup_widget(window1,"hscale1");
	mode=2;
	gtk_range_set_value (GTK_RANGE(scale),0.0);
}


void
on_button_play_released                (GtkButton       *button,
                                        gpointer         user_data)
{
	jack_play();
}


void
on_button_pause_released               (GtkButton       *button,
                                        gpointer         user_data)
{
	jack_stop();
}


void
on_button_ff_pressed                   (GtkButton       *button,
                                        gpointer         user_data)
{
	fastskip=1.0;
	gfastskip(NULL);
	g_timeout_add(120,gfastskip,NULL);
}


void
on_button_ff_released                  (GtkButton       *button,
                                        gpointer         user_data)
{
	fastskip=0.0;
#if 0
	GtkWidget *scale = lookup_widget(window1,"hscale1");
	mode=2;
	double val = gtk_range_get_value (GTK_RANGE(scale));
	gtk_range_set_value (GTK_RANGE(scale),val+40.0);
#endif
}


void
on_button_end_released                 (GtkButton       *button,
                                        gpointer         user_data)
{
	GtkWidget *scale = lookup_widget(window1,"hscale1");
	mode=2;
	gtk_range_set_value (GTK_RANGE(scale),999.0);
}


void
on_button_fr_pressed                   (GtkButton       *button,
                                        gpointer         user_data)
{
	fastskip=-1.0;
	gfastskip(NULL);
	g_timeout_add(120,gfastskip,NULL);
}


void
on_button_fr_released                  (GtkButton       *button,
                                        gpointer         user_data)
{
	fastskip=0.0;
#if 0
	GtkWidget *scale = lookup_widget(window1,"hscale1");
	mode=2;
	double val = gtk_range_get_value (GTK_RANGE(scale));
	gtk_range_set_value (GTK_RANGE(scale),val-40.0);
#endif
}


void
on_button6_released                    (GtkButton       *button,
                                        gpointer         user_data)
{
	GtkWidget *spbtn1 = lookup_widget(window1,"spinbutton1");
	GtkWidget *spbtn2 = lookup_widget(window1,"spinbutton2");

	double val = jack_poll_time();
	if (val < 0 ) {dothejack(0); return;}
	if (z_scale == 0.0) {
		stg[mem].start=0.0;
		stg[mem].end   = val * 100.0 / z_pos;
		if (stg[mem].end==stg[mem].start) stg[mem].end+=60.0;
	} else {
		stg[mem].start = val - (z_scale / 100.0 * z_pos);
		stg[mem].end   = val + (z_scale / 100.0 * (100.0-z_pos));
	}

	if (stg[mem].start < 0.0 ) stg[mem].start=0.0;

	gtk_spin_button_set_value (GTK_SPIN_BUTTON(spbtn2),stg[mem].end/unit_table[stg[mem].unit]);
	gtk_spin_button_set_value (GTK_SPIN_BUTTON(spbtn1),stg[mem].start/unit_table[stg[mem].unit]);
}

void
on_combobox1_changed                   (GtkComboBox     *combobox,
                                        gpointer         user_data)
{
	guint sel = gtk_combo_box_get_active (combobox);
	if (sel+3 >= UNIT_LAST ) return;
	update_startend(4+ sel);
}


void
on_combobox2_changed                   (GtkComboBox     *combobox,
                                        gpointer         user_data)
{
	GtkWidget *w=lookup_widget(window1,"combobox1");
	guint sel = gtk_combo_box_get_active (combobox);

	if (sel > 4 ) return;
	if (sel < 0 ) return;
	gtk_widget_set_sensitive(w,(sel==3));

	if (sel < 3 ) { 
		update_startend(sel);
		return;
	}
	if (sel == 4 ) { 
		update_startend(UNIT_AUDIOFRAMES);
		return;
	} else { 
		sel = gtk_combo_box_get_active (GTK_COMBO_BOX(w));
		if (sel+3 >= UNIT_LAST ) return;
		update_startend(4+ sel);
	}
}


void
on_button_pos_released                 (GtkButton       *button,
                                        gpointer         user_data)
{
	jack_reposition(0);
}


void
on_combobox3_changed                   (GtkComboBox     *combobox,
                                        gpointer         user_data)
{
	 switch(gtk_combo_box_get_active (combobox)) {
	 	case 1: z_scale=1.0; break;
	 	case 2: z_scale=5.0; break;
	 	case 3: z_scale=10.0; break;
	 	case 4: z_scale=60.0; break;
	 	case 5: z_scale=300.0; break;
	 	case 6: z_scale=900.0; break;
	 	default: z_scale=0.0; 
	 }
}


void
on_combobox4_changed                   (GtkComboBox     *combobox,
                                        gpointer         user_data)
{
	 switch(gtk_combo_box_get_active (combobox)) {
	 	case 1: z_pos=50.0; break;
	 	case 2: z_pos=90.0; break;
	 	case 3: z_pos=100.0; break;
	 	default: z_pos=10.0;
	 }
}


void
on_checkbutton4_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
	GtkWidget *w=lookup_widget(window1,"handlebox1");
	if ( gtk_toggle_button_get_active(togglebutton)) 
		gtk_widget_show (w);
	else  
		gtk_widget_hide (w);
	minimize_height();
}


void
on_checkbutton2_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
	GtkWidget *w=lookup_widget(window1,"handlebox2");
	if ( gtk_toggle_button_get_active(togglebutton)) 
		gtk_widget_show (w);
	else  
		gtk_widget_hide (w);
	minimize_height();
}


void
on_checkbutton5_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
	GtkWidget *w=lookup_widget(window1,"statusbar1");
	if ( gtk_toggle_button_get_active(togglebutton)) 
		gtk_widget_show (w);
	else  
		gtk_widget_hide (w);
	minimize_height();
}


void
on_checkbutton3_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
	GtkWidget *w=lookup_widget(window1,"handlebox3");
	if ( gtk_toggle_button_get_active(togglebutton)) 
		gtk_widget_show (w);
	else  
		gtk_widget_hide (w);
	minimize_height();
}


void
on_checkbutton6_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
	GtkWidget *w=lookup_widget(window1,"table1");
	if ( gtk_toggle_button_get_active(togglebutton)) {
		gtk_widget_show (w);
		status_printf("");
	} else {
		gtk_widget_hide (w);
		// TODO: parse/print unit.
		status_printf("minimized. slider-range: %.2f .. %.2f sec",stg[mem].start,stg[mem].end);
	}
	minimize_height();
}


void
on_combobox5_changed                   (GtkComboBox     *combobox,
                                        gpointer         user_data)
{
	int oldmem = mem;
	int t=gtk_combo_box_get_active (combobox);
	if (t < 0 || t>= max_mem) return;
	mem=t;
	if (oldmem == mem) return;

	// update start + end time
	GtkWidget *spbtn1 = lookup_widget(window1,"spinbutton1");
	GtkWidget *spbtn2 = lookup_widget(window1,"spinbutton2");
	GtkWidget *w;
	gdouble min,max;

	gtk_spin_button_get_range (GTK_SPIN_BUTTON(spbtn2), &min, &max);
	gtk_spin_button_set_range (GTK_SPIN_BUTTON(spbtn2), 0, max);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(spbtn2),stg[mem].end/unit_table[stg[mem].unit]);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(spbtn1),stg[mem].start/unit_table[stg[mem].unit]);
	
	// select fps unit
	w = lookup_widget(window1,"combobox1");
	if (stg[mem].unit>3)
		gtk_combo_box_set_active (GTK_COMBO_BOX(w),(stg[mem].unit>3)?stg[mem].unit-4:6);
	gtk_widget_set_sensitive(w,(stg[mem].unit>3));

	// select units
	w = lookup_widget(window1,"combobox2");
	if (stg[mem].unit==3) gtk_combo_box_set_active (GTK_COMBO_BOX(w),4);  // Audioframes
	else gtk_combo_box_set_active (GTK_COMBO_BOX(w),(stg[mem].unit<3)?stg[mem].unit:3); 

	// (also remember fit params ??)

}


void
on_checkbutton7_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
	GtkWidget *w=lookup_widget(window1,"handlebox4");
	if ( gtk_toggle_button_get_active(togglebutton)) 
		gtk_widget_show (w);
	else  
		gtk_widget_hide (w);
	minimize_height();
}


void
on_checkbutton8_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
	GtkWidget *w=lookup_widget(window1,"hbox_labels");
	if ( gtk_toggle_button_get_active(togglebutton)) 
		gtk_widget_show (w);
	else  
		gtk_widget_hide (w);
	minimize_height();
}

void
on_button7_released                    (GtkButton       *button,
                                        gpointer         user_data)
{
	showhide("checkbutton5","statusbar1",1);
	status_printf("gjacktransport %s  (C) GPL 2007, 2010 Robin Gareus  http://gjacktransport.sf.net/", VERSION);
}


void
on_button_smpte_released               (GtkButton       *button,
                                        gpointer         user_data)
{
	GtkWidget *p = create_dialog_smpte();
	set_dialog_smpte(p, 
		(GTK_WIDGET(button)==lookup_widget(window1,"button_smpte_start"))?
			stg[mem].start : stg[mem].end);
	if (gtk_dialog_run (GTK_DIALOG (p)) == GTK_RESPONSE_OK) {
		gdouble df = parse_dialog_smpte(p);
		apply_smpte((GTK_WIDGET(button)==lookup_widget(window1,"button_smpte_start"))?
			&stg[mem].start : &stg[mem].end, df);
	}
	const char *n = gtk_entry_get_text(GTK_ENTRY(lookup_widget(p,"entry1")));
	if (stg[mem].name) free(stg[mem].name);
	stg[mem].name = NULL;
	if (strlen(n) > 0) stg[mem].name = strdup(n);
	update_combotxt(mem);
  gtk_combo_box_set_active (GTK_COMBO_BOX(lookup_widget(window1,"combobox5")),mem);
	gtk_widget_destroy (p);
}


void
on_radiobuttonS_jack_toggled           (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
	gtk_widget_set_sensitive(GTK_WIDGET(togglebutton),!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(user_data)));
	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(user_data)))
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(user_data),(jffr->flags&FRF_DROP_FRAMES));
}


void
on_radiobuttonS_vf_toggled             (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
	gtk_widget_set_sensitive(GTK_WIDGET(togglebutton),gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(user_data)));
	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(user_data)))
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(user_data),(uffr->flags&FRF_DROP_FRAMES));
}


gboolean
on_main_key_press_event                (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data)
{
	gboolean rv = FALSE;
	if(event->keyval==GDK_Escape) return (rv);

  if(event->state&GDK_CONTROL_MASK && (event->keyval==GDK_q || event->keyval==GDK_w)) {
    gtk_main_quit();
  }

	if(event->keyval==key_cfg[0]) {
		int val = jack_poll_transport_state();
		if (val&1) jack_stop();
		else if (val&2) jack_play();
		status_printf("Key: toggle play/pause");
		rv = TRUE;
	} else if(event->keyval==key_cfg[1])  {
		fastskip=-50.0; // 5% of slider range
		gfastskip(NULL);
		fastskip=0.0; g_timeout_add(120,gfastskip,NULL);
		status_printf("Key: fast rewind");
		rv = TRUE;
	} else if(event->keyval==key_cfg[2])  {
		fastskip=50.0; // 5% of slider range
		gfastskip(NULL);
		fastskip=0.0; g_timeout_add(120,gfastskip,NULL);
		status_printf("Key: fast forward");
		rv = TRUE;
	} else if(event->keyval==key_cfg[3])  {
		jack_play();
		status_printf("Key: play trasport");
		rv = TRUE;
	} else if(event->keyval==key_cfg[4])  {
		jack_stop();
		status_printf("Key: pause trasport");
		rv = TRUE;
	} else if(event->keyval==key_cfg[5])  {
		on_button_rewind_released(NULL,NULL);
		mode=0;
		status_printf("Key: instant rewind");
		rv = TRUE;
	} else if(event->keyval==key_cfg[6])  {
		seek_relative(-1*stride);
		status_printf("Key: seek backward");
		rv = TRUE;
	} else if(event->keyval==key_cfg[7])  {
		seek_relative(stride);
		status_printf("Key: seek forward");
		rv = TRUE;
	} else if(accept_num && (event->keyval>=GDK_1 && event->keyval<=GDK_6)) {
		int mi = event->keyval-GDK_1;
		select_mem(mi);
		status_printf("Key: switching to memory: %c",'A'+mi);
		rv = TRUE;
	} else status_printf("");

  return rv;
}


void
on_buttonK0_released                   (GtkButton       *button,
                                        gpointer         user_data)
{
	prepare_key(0);
}


void
on_buttonK1_released                   (GtkButton       *button,
                                        gpointer         user_data)
{
	prepare_key(1);
}


void
on_buttonK2_released                   (GtkButton       *button,
                                        gpointer         user_data)
{
	prepare_key(2);
}


void
on_buttonK3_released                   (GtkButton       *button,
                                        gpointer         user_data)
{
	prepare_key(3);
}


void
on_buttonK4_released                   (GtkButton       *button,
                                        gpointer         user_data)
{
	prepare_key(4);
}


void
on_buttonK5_released                   (GtkButton       *button,
                                        gpointer         user_data)
{
	prepare_key(5);
}


void
on_buttonK6_released                   (GtkButton       *button,
                                        gpointer         user_data)
{
	prepare_key(6);
}


void
on_buttonK7_released                   (GtkButton       *button,
                                        gpointer         user_data)
{
	prepare_key(7);
}

void
on_button_loadrc_released              (GtkButton       *button,
                                        gpointer         user_data)
{
	GtkWidget *dialog;
	dialog = gtk_file_chooser_dialog_new ("Load Config", GTK_WINDOW(window1),
			GTK_FILE_CHOOSER_ACTION_OPEN, GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
			GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT, NULL);
	if (rc_file) {
		gtk_file_chooser_set_filename (GTK_FILE_CHOOSER (dialog), rc_file);
	}
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
		char *filename;
		filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
		if (filename) {
			try_load_rc_file(filename);
			post_rc_update();
			if (rc_file) free(rc_file);
			rc_file=strdup(filename);
			g_free (filename);
		}
	}
	gtk_widget_destroy (dialog);
}

void
on_button_saverc_released              (GtkButton       *button,
                                        gpointer         user_data)
{
	if (!rc_file) {
		on_button_saveasrc_released(button, user_data);
		return;
	}
	write_rc(rc_file);
}

void
on_button_saveasrc_released              (GtkButton       *button,
                                        gpointer         user_data)
{
	GtkWidget *dialog;
	dialog = gtk_file_chooser_dialog_new ("Save Config", GTK_WINDOW(window1),
			GTK_FILE_CHOOSER_ACTION_SAVE, GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
			GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT, NULL);

	gtk_file_chooser_set_do_overwrite_confirmation (GTK_FILE_CHOOSER (dialog), TRUE);
	if (rc_file) {
		gtk_file_chooser_set_filename (GTK_FILE_CHOOSER (dialog), rc_file);
	} else {
		gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (dialog), getenv("HOME"));
		gtk_file_chooser_set_current_name (GTK_FILE_CHOOSER (dialog), ".gjacktransportrc"); // XXX
	}

	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT) {
		char *filename;
		filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));
		if (filename) {
			if (rc_file) free(rc_file);
			rc_file=strdup(filename);
			write_rc(rc_file);
			g_free (filename);
		}
	}
	gtk_widget_destroy (dialog);
}

void
on_button_keyprefs_released            (GtkButton       *button,
                                        gpointer         user_data)
{
	GtkWidget *p = create_dialog_key();
	// update labels
	key_dlg = p;
	key_update=-1;
	int i;
	for (i=0; i<MAX_KEYBINDING; i++) { 
		char kxt[20];
		snprintf(kxt,20,"labelK%i",i);
		GtkLabel *bl = GTK_LABEL(lookup_widget(key_dlg,kxt));
		
		if (key_cfg[i]==GDK_Escape) {
			snprintf(kxt,20,": [off]");
		} else if (key_cfg[i]==GDK_space) {
			snprintf(kxt,20,": [Space-bar]");
		} else if (key_cfg[i]==GDK_Return) {
			snprintf(kxt,20,": [Enter]");
		} else if (key_cfg[i]>0 && key_cfg[i]< 256) {
			snprintf(kxt,20,": '%c'", key_cfg[i]&0xff);
		} else {
			snprintf(kxt,20,": [non-print-char]");
		}
		gtk_label_set_text(GTK_LABEL(bl),kxt);
	}
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(lookup_widget(key_dlg,"spinbutton3")),stride);
	gtk_label_set_text(GTK_LABEL(lookup_widget(key_dlg,"label_status")),"-");
	gtk_dialog_run (GTK_DIALOG (p));
	gtk_widget_destroy (p);
}

gboolean
on_dialog_key_key_press_event          (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data)
{
	if (key_update!=-1) {
		set_key(event->keyval);
		return TRUE;
	}
  return FALSE;
}

gboolean
on_spinbutton_focus_in_event           (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data)
{
  accept_num=0;
  return FALSE;
}


gboolean
on_spinbutton_focus_out_event          (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data)
{
  accept_num=1;
  return FALSE;
}


void
on_spinbutton_stride_changed           (GtkEditable     *editable,
                                        gpointer         user_data)
{	
	char txt[64];
	stride = gtk_spin_button_get_value (GTK_SPIN_BUTTON(editable));
	GtkLabel *l = GTK_LABEL(lookup_widget(key_dlg,"label_status"));
	snprintf(txt,64,"updated skip stride: %.2f", stride);
	gtk_label_set_text(GTK_LABEL(l),txt);
}
