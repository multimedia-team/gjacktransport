/* gjackclock - jack transport big clock
 *  Copyright (C) 2010 Robin Gareus  <robin@gareus.org>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <getopt.h>
#include <math.h>
#include <jack/jack.h>
#include <jack/transport.h>

#include "framerate.h"
#include "gjackclock.h"

#include <libintl.h>
#define _(X) gettext(X)

/* globals */
char *program_name;

static GdkPixmap *gjc_icon_pixmap;
static GdkBitmap *gjc_icon_mask;
#include "gjc_icon.xpm"

GtkWidget *window1;
GtkWidget *labelSMPTE[12];
GtkWidget *label1 = NULL;
GtkWidget *label2;
FrameRate *fr = NULL;
int vboxw = 0;

/* command-line args */
int want_quiet = 0;
int want_ontop = 0;
int want_scaling = 1;
int show_fps = 1;
char *font_family = NULL;
int font_size = 44;
double user_fps = 25;
int want_ignore_timemaster = 0;
char *win_pos = NULL;
double stride = 10;
char *color_bg = NULL;
char *color_fg = NULL;
int use_table_layout = 0;
char *rc_file = NULL;

gint key_cfg[MAX_KEYBINDING] = { GDK_space, GDK_v, GDK_n, GDK_b, GDK_p, GDK_r, GDK_less, GDK_greater };


/*****************************************************************************
 * JACK FUNCTIONS
 */

jack_client_t *jack_client = NULL;
jack_nframes_t j_srate = 48000;
char jackid[16];

/* when jack shuts down... */
void jack_shutdown(void *arg) {
  jack_client=NULL;
}

int jack_connected(void) {
  if (jack_client) return (1);
  return (0);
}

void open_jack(void ) {
  if (jack_client) {
    if (!want_quiet)
      fprintf (stderr, "already connected to jack..\n");
    return;
  }

  int i=0;
  do {
    snprintf(jackid,16,"gjackclock-%i",i);
    jack_client = jack_client_open (jackid, JackUseExactName, NULL);
  } while (jack_client == 0 && i++<16);

  if (!jack_client) {
    fprintf(stderr, "could not connect to jack server.\n");
  } else { 
    jack_on_shutdown (jack_client, jack_shutdown, 0);
    if (!want_quiet) 
      fprintf(stderr, "connected as jack client '%s'\n",jackid);
    jack_activate (jack_client);
  }
}

void close_jack(void) {
  if (jack_client) {
    jack_deactivate (jack_client);
    jack_client_close (jack_client);
    if (!want_quiet) 
      fprintf(stderr, "disconnected from jack.\n");
  }
  jack_client=NULL;
}

void jack_stop(void) {
  if (jack_client) {
    jack_transport_stop (jack_client);
  }
}

void jack_play(void) {
  if (jack_client) {
    jack_transport_start (jack_client);
  }
}

int jack_poll_transport_state (void) {
  if (jack_client) 
    switch (jack_transport_query(jack_client, NULL)) {
      case JackTransportRolling:
        return 1|4;
      case JackTransportStopped:
        return 2|4;
      default:
        return 3|4;
    }
  return 0;
}

long long int jack_poll_time (void) {
  jack_position_t  jack_position;
  if (!jack_client) return (-1);

  jack_transport_query(jack_client, &jack_position);
  FR_setsamplerate(fr, jack_position.frame_rate);

  if (jack_position.valid&JackAudioVideoRatio && !want_ignore_timemaster) {
    double jfps= jack_position.frame_rate / (double) jack_position.audio_frames_per_video_frame;
    FR_setdbl(fr,jfps,1);
  }
  return (jack_position.frame);
}

void jack_reposition(jack_nframes_t f) {
  if (!jack_client) return;
  jack_transport_locate (jack_client, f);
}

void jack_relative_reposition(double off) {
  if (!jack_client) return;
  long long int frame = jack_poll_time();
  jack_nframes_t f = 0;
  frame += off * fr->samplerate;
  if (frame > 0) f = frame;
  jack_reposition(f);
}

/*****************************************************************************/

static void usage (int status) {
  printf("%s - JACK transport big clock.\n",program_name);
  printf("usage: %s [Options]\n",program_name);
  printf (""
"Options:\n"
"  -a, --ontop               start 'always-on-top' of other windows\n"
"  -C <color>                background-color ('#RRGGBB' or name eg 'black')\n"
"  -c <color>                text-color ('#RRGGBB' or name eg 'blue')\n"
"  -d <sec>                  set seek-distance (default 10.0 sec)\n"
"  -f <FPS>, --fps <FPS>     set timecode-fps (default 25 or timebase-master)\n"
"  -h, --help                display this help and exit\n"
"  -I, --ignore-timemaster   ignore JACK-timebase-master FPS setting\n"
"  -l <file>,\n"
"    --config <file>         load configuration from <file>\n"
"  -N, --no-scale            do not scale font when resizing the window\n"
"  -n, --no-fps-display      hide FPS info from display\n"
"  -q, --quiet, --silent     inhibit usual output\n"
"  -S <FONT-Family>          default 'Monospace'\n"
"  -s <FONT-size>            default 60\n"
"  -t, --table-layout        render time in equally spaced table (use with\n"
"                            variable width fonts)\n"
"  -V, --version             print version information and exit\n"
"  -w <XOFFxYOFF>            set window position (eg '100x200')\n"
""
"Notes:\n"
"  * use -f 29.97 for drop-frame timecode\n"
"  * default key-bindings (override with ~/.gjackclockrc)\n"
"    'space'   - toggle play/pause\n"
"    'p'       - pause\n"
"    'b'       - play\n"
"    'r'       - rewind\n"
"    '<'       - skip backward\n"
"    '>'       - skip forward\n"
"   CTRL-[q|w] - quit\n"
""
);
  exit(status);
}

static void printversion (void) {
  printf ("gjackclock %s\n (C) GPL 2010 Robin Gareus <robin@gareus.org>\n", VERSION);
}

static struct option const long_options[] =                                                                                                    
{
  {"help", no_argument, 0, 'h'},
  {"quiet", no_argument, 0, 'q'},
  {"silent", no_argument, 0, 'q'},
  {"fps", required_argument, 0, 'f'},
  {"ignore-timemaster", no_argument, 0, 'I'},
  {"font-size", required_argument, 0, 's'},
  {"font-family", required_argument, 0, 'S'},
  {"window-position", required_argument, 0, 'w'},
  {"no-fps-display", no_argument, 0, 'n'},
  {"no-scale", no_argument, 0, 'N'},
  {"ontop", no_argument, 0, 'a'},
  {"table-layout", no_argument, 0, 't'},
  {"version", no_argument, 0, 'V'},
  {"config", required_argument, 0, 'l'},
  {NULL, 0, NULL, 0}
};

static int
decode_switches (int argc, char **argv)
{
  int c;
  while ((c = getopt_long (argc, argv, 
                           "q"  /* quiet or silent */
                           "h"  /* help */
                           "f:" /* fps */
                           "s:" /* font-size */
                           "S:" /* font-family */
                           "a"  /* ontop */
                           "w:" /* window-pos */
                           "c:" /* color-fg */
                           "C:" /* color-bg */
                           "d:" /* stride-dist */
                           "I"  /* ignore time-master */
                           "t"  /* table-layout */
                           "N"  /* no-scale */
                           "n"  /* no-fps-display */
                           "l:" /* config file */
                           "V", /* version */
                           long_options, (int *) 0)) != EOF)
  {
    switch (c) {
        case 'q':
          want_quiet = 1;
          break;
        case 'I':
          want_ignore_timemaster = 1;
          break;
        case 'f':
          user_fps = atof(optarg);
          break;
        case 's':
          font_size = atoi (optarg);
          if (font_size < 6 || font_size > 500) font_size = 60;
          break;
        case 'S':
          if (font_family) free(font_family);
          font_family = strdup(optarg);
          break;
        case 'c':
          if (color_fg) free(color_fg);
          color_fg = strdup(optarg);
          break;
        case 'C':
          if (color_bg) free(color_bg);
          color_bg = strdup(optarg);
          break;
        case 'd': // XXX 'd' ??
          stride = atof(optarg);
          if (stride <=0) stride = 10.0;
          break;
        case 'w':
          if (win_pos) free(win_pos);
          win_pos = strdup(optarg);
          break;
        case 'l':
          if (rc_file) free(rc_file);
          rc_file = strdup(optarg);
          break;
        case 'a':
          want_ontop = 1;
          break;
        case 'N':
          want_scaling = 0;
          break;
        case 'n':
          show_fps = 0;
          break;
        case 't':
          use_table_layout = 1;
          break;
        case 'V':                                                                                                                              
          printversion();
          exit(0);
        case 'h':
          usage (0);
        default:
          usage (EXIT_FAILURE);
    }
  }
  return optind;
}

/*****************************************************************************/
void set_smpte_font (char *font) {
  if (use_table_layout) {
    int i;
    for (i=0;i<12;i++) {
      gtk_widget_modify_font (labelSMPTE[i], pango_font_description_from_string (font));
    }
  } else {
    gtk_widget_modify_font (label1, pango_font_description_from_string (font));
  }
}

void set_smpte_text (gchar *txt) {
  if (use_table_layout) {
    int i;
    int len=strlen(txt);
    for (i=0;i<12;i++) {
      gchar st[2];
      memset(&st,0,2);
      if (i<len) st[0]= txt[i];
      gtk_label_set_text(GTK_LABEL(labelSMPTE[i]), st);
    }
  } else {
    gtk_label_set_text(GTK_LABEL(label1),txt);
  }
}

gboolean gpolljack (gpointer data) {
  gchar text[16];
  char fpstxt[16];
  long long int jt = jack_poll_time();
  if (jt < 0 ) { open_jack(); return (TRUE);}

  long int frame =  FR_af2vfi(fr, jt);
  if (frame < 0) snprintf(text, 16 , "--:--:--.--");
  else FR_vf2smpte(fr, text, frame);
  set_smpte_text(text);

  snprintf(fpstxt,16,"%.2f", FR_todbl(fr));
  gtk_label_set_text(GTK_LABEL(label2),fpstxt);
  return(TRUE);
}

/*****************************************************************************/
gboolean
on_key_press_event (GtkWidget       *widget,
                    GdkEventKey     *event,
                    gpointer         user_data)
{
  gboolean rv = FALSE;
  if(event->keyval==GDK_Escape) return (rv);

  if(event->state&GDK_CONTROL_MASK && (event->keyval==GDK_q || event->keyval==GDK_w)) {
    gtk_main_quit();
  }

  if(event->keyval==key_cfg[0]) {
    int val = jack_poll_transport_state();
    if (val&1) jack_stop();
    else if (val&2) jack_play();
    rv = TRUE;
  } else if(event->keyval==key_cfg[3])  {
    jack_play();
    rv = TRUE;
  } else if(event->keyval==key_cfg[4])  {
    jack_stop();
    rv = TRUE;
  } else if(event->keyval==key_cfg[5])  {
    jack_reposition(0);
    rv = TRUE;
  } else if(event->keyval==key_cfg[6])  {
    jack_relative_reposition(-1.0*stride);
    rv = TRUE;
  } else if(event->keyval==key_cfg[7])  {
    jack_relative_reposition(stride);
    rv = TRUE;
  } 
#if 0
  else {
    printf("KEY key:%x state:%x\n", event->keyval, event->state);
  }
#endif

  return rv;
}

gint orig_win_width = -1;
gint orig_win_height = -1;
#define MINIMUM(a,b) ((a>b)?(b):(a))
/* concept: see 'idle_big_clock_text_resizer() in gtk2_ardour/ardour_ui_ed.cc 
 * Thanks to Paul Davis */
gboolean gfontresize (gpointer data) {
  static gint win_width = -1;
  static gint win_height = -1;
  static int cur_font_size = -1;
  char font[1024];
  gint my_width, my_height;
  gtk_window_get_size (GTK_WINDOW(window1), &my_width, &my_height);

  if (win_width < 0 || win_height < 0 ) {
    win_width = my_width;
    win_height = my_height;
    return TRUE;
  }
  if (win_width == my_width && win_height == my_height ) {
    return TRUE;
  }

  double scale = MINIMUM((double) (my_width-vboxw) / (double) (orig_win_width-vboxw),
                         (double) my_height / (double) orig_win_height);
  int my_font_size = floor((double) font_size * scale);
  if (my_font_size < 4) my_font_size=4;
  if (cur_font_size == my_font_size ) return TRUE;

  snprintf(font, 1024, "%s %i",font_family?font_family:"Monospace", my_font_size);
  set_smpte_font(font);
  cur_font_size = my_font_size;
  return TRUE;
}

/*****************************************************************************/


int main (int argc, char *argv[]) {
  int i;
  GtkWidget *table1 = NULL;
  GtkWidget *hbox1, *vbox1, *vbox2;
  GtkWidget *alignment1;
  GtkWidget *label3;
  char font[1024];

  program_name = argv[0];

#ifdef ENABLE_NLS
  bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);
#endif

  gtk_init(&argc, &argv);

  window1 = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window1), _("gjackclock"));

  hbox1 = gtk_hbox_new (FALSE, 8);
  vbox1 = gtk_vbox_new (FALSE, 0);
  vbox2 = gtk_vbox_new (FALSE, 0);
  alignment1 = gtk_alignment_new (.5, .5, 0, 0);
  label2 = gtk_label_new ("");
  label3 = gtk_label_new (_("fps:"));

  load_rc();

  i = decode_switches (argc, argv);
  if (argc != i) usage(1);

  if(rc_file) try_load_rc_file(rc_file);

  fr = FR_create(25,1,FRF_NONE);
  FR_setdbl(fr,user_fps,1);

  if (use_table_layout) {
    table1 = gtk_table_new (1,12, TRUE);
    for (i=0;i<12;i++) {
      labelSMPTE[i] = gtk_label_new ("--");
      //gtk_widget_set_size_request(labelSMPTE[i],30,-1);
      gtk_table_attach (GTK_TABLE (table1), labelSMPTE[i],i, i+1, 0, 1,
          (GtkAttachOptions) (GTK_EXPAND), 
          (GtkAttachOptions) (GTK_FILL), 0, 0);
    }
    set_smpte_text(_("HH:MM:SS:FFF"));
    gtk_box_pack_start (GTK_BOX (vbox2), table1, TRUE, FALSE, 0);
  } else {
    label1 = gtk_label_new (_("HH:MM:SS:FFF"));
    gtk_box_pack_start (GTK_BOX (vbox2), label1, TRUE, FALSE, 0);
  }

  gtk_container_add (GTK_CONTAINER (window1), hbox1);
  gtk_box_pack_start (GTK_BOX (hbox1), vbox2, TRUE, TRUE, 0);
  gtk_box_pack_start (GTK_BOX (hbox1), alignment1, FALSE, FALSE, 4);
  gtk_container_add (GTK_CONTAINER(alignment1), vbox1);
  gtk_box_pack_start (GTK_BOX (vbox1), label3, FALSE, FALSE, 2);
  gtk_box_pack_start (GTK_BOX (vbox1), label2, FALSE, FALSE, 2);

  snprintf(font, 1024, "%s %i",font_family?font_family:"Monospace", font_size);
  set_smpte_font(font);

  if (color_bg) {
    GdkColor color;
    gdk_color_parse (color_bg, &color);
    gtk_widget_modify_bg (window1, GTK_STATE_NORMAL, &color);
  }
  if (color_fg) {
    GdkColor color;
    gdk_color_parse (color_fg, &color);
    if (use_table_layout) {
      for (i=0;i<12;i++) {
        gtk_widget_modify_fg (labelSMPTE[i], GTK_STATE_NORMAL, &color);
      }
    } else {
      gtk_widget_modify_fg (label1, GTK_STATE_NORMAL, &color);
    }
    gtk_widget_modify_fg (label2, GTK_STATE_NORMAL, &color);
    gtk_widget_modify_fg (label3, GTK_STATE_NORMAL, &color);
  }

  if (use_table_layout) {
    gtk_widget_show (table1);
    for (i=0;i<12;i++) {
      gtk_widget_show (labelSMPTE[i]);
    }
  } else {
    gtk_widget_show (label1);
  }
  gtk_widget_show (label2);
  gtk_widget_show (label3);
  gtk_widget_show (hbox1);
  gtk_widget_show (vbox2);
  if (show_fps) {
    gtk_widget_show (alignment1);
    gtk_widget_show (vbox1);
#if 0 /* GTK-3.0 */
    GtkRequisition rx;
    gtk_widget_get_preferred_size(GTK_WIDGET(alignment1), &rx, NULL);
    vboxw = rx.width;
    if (vboxw < 0) vboxw = 32;
#else
    vboxw = 32; // guess :)
#endif
  }

  if (want_scaling) {
    GdkGeometry geo; 
    gtk_window_get_size (GTK_WINDOW(window1), &geo.min_width, &geo.min_height);
    gtk_window_set_geometry_hints(GTK_WINDOW (window1), NULL, &geo, GDK_HINT_MIN_SIZE);
  }

  gtk_widget_show (window1);

  gjc_icon_pixmap = gdk_pixmap_create_from_xpm_d (window1->window, &gjc_icon_mask, NULL, gjc_icon_xpm);
  gdk_window_set_icon(window1->window, NULL, gjc_icon_pixmap, gjc_icon_mask);
  g_signal_connect ((gpointer) window1, "destroy", G_CALLBACK (gtk_main_quit), NULL);
  g_signal_connect ((gpointer) window1, "key_press_event", G_CALLBACK (on_key_press_event), NULL);

  if (win_pos) {
    char *t = strchr(win_pos, 'x');
    int win_x = atoi(win_pos);
    int win_y = -1;
    if (t) win_y = atoi(t+1);
    if (!want_quiet) 
      printf("window position: %ix%i \n", win_x, win_y);
    if (win_x>=0 && win_y >=0 ) {
      gtk_window_set_gravity(GTK_WINDOW(window1), GDK_GRAVITY_STATIC);
      gtk_window_move(GTK_WINDOW(window1), win_x, win_y);
    }
  }

  if (want_ontop) 
    gtk_window_set_keep_above(GTK_WINDOW(window1), 1);

  gtk_window_get_size(GTK_WINDOW(window1), &orig_win_width, &orig_win_height);

  g_timeout_add(40,gpolljack,NULL); // TODO: update MAX(1/FPS , screen update freq)

  if (want_scaling) 
    g_timeout_add(80,gfontresize,NULL); 

  gtk_main ();

  if (color_fg) free(color_fg);
  if (color_bg) free(color_bg);
  if (fr) FR_free(fr);
  if (font_family) free(font_family);
  if (win_pos) free(win_pos);
  close_jack();
  return 0;
}

/* vi:set ts=8 sw=2 et: */ 
