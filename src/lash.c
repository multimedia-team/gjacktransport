/* gjacktransport - LASH interface
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 *
 * (c) 2007 
 *  Robin Gareus <robin@gareus.org>
 *
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include "gjack.h"
#include "gjacktransport.h"
#include "framerate.h"

extern FrameRate *fffr;
extern FrameRate *uffr;
extern FrameRate *jffr;

extern gint key_cfg[MAX_KEYBINDING];
extern int max_mem;
extern int no_resize;

#ifdef HAVE_LASH
# include <lash/lash.h>
//------------------------------------------------
// extern Globals (main.c)
//------------------------------------------------
// defined in main.c
extern lash_client_t *lash_client;
extern int mem;
extern double stride;
extern gjtdata *stg;
extern GtkWidget *window1;

extern int want_quiet;

/* restore config functions - see callbacks.c */
void select_mem (int newmem);
void showhide (char *togglebtn, char *widget, gboolean display);
gboolean ishidden (char *widget); //toggle-btn name
void dothejack (gboolean onoff);
void update_combotxt (int pos);
int realloc_mem (int newmax);

// LASH extension - long int - use with lcs_long()
long int lash_config_get_value_long (const lash_config_t * config) {
	const void *data = lash_config_get_value(config);
	return(*((long*) data));
}

/*************************************
 * lcs_*() - LASH Config Set functions 
 */

void lcs_str(char *key, char *value) {
#ifdef HAVE_LASH
	lash_config_t *lc = lash_config_new_with_key(key);
	lash_config_set_value_string (lc, value);
	lash_send_config(lash_client, lc);
	//printf("DEBUG - LASH config str: %s -> %i\n",key,value);
#endif
}

void lcs_long(char *key, long int value) {
#ifdef HAVE_LASH
	lash_config_t *lc;
	lc = lash_config_new_with_key(key);
	lash_config_set_value (lc, (void*) &value, sizeof(long int));
	lash_send_config(lash_client, lc);
	//printf("DEBUG - LASH config long %ld -> %i\n",key,value);
#endif
}

void lcs_int(char *key, int value) {
#ifdef HAVE_LASH
	lash_config_t *lc;
	lc = lash_config_new_with_key(key);
	lash_config_set_value_int (lc, value);
	lash_send_config(lash_client, lc);
	//printf("DEBUG - LASH config int: %i -> %i\n",key,value);
#endif
}

void lcs_dbl(char *key, double value) {
#ifdef HAVE_LASH
	lash_config_t *lc;
	lc = lash_config_new_with_key(key);
	lash_config_set_value_double (lc, value);
	lash_send_config(lash_client, lc);
	//printf("DEBUG - LASH config dbl %g -> %i\n",key,value);
#endif
}

/*************************
 * private LASH functions
 */
gint my_root_x, my_root_y, my_width, my_height;

void handle_event(lash_event_t* ev) {
	int type = lash_event_get_type(ev);
	const char*	str = lash_event_get_string(ev);
	
	if (type == LASH_Restore_Data_Set) {
		// FIXME - send this AFTER recv. config
		if (!want_quiet)
			printf("LASH restore data set\n");
		//lash_send_event(lash_client, lash_event_new_with_type(LASH_Restore_Data_Set));
	} else if (type == LASH_Save_Data_Set) {
		int i;
		if (!want_quiet)
			printf("LASH saving data set\n");

		if (jack_connected())  lcs_int("syncsource",1);
		else lcs_int("syncsource",0);

		for (i=0; i< max_mem; i++) {
			char tmp[32];
			snprintf(tmp,31,"memory_%02i_start",i+1);
			lcs_dbl(tmp,stg[i].start);
			snprintf(tmp,31,"memory_%02i_end",i+1);
			lcs_dbl(tmp,stg[i].end);
			snprintf(tmp,31,"memory_%02i_unit",i+1);
			lcs_int(tmp,stg[i].unit);
			snprintf(tmp,31,"memory_%02i_name",i+1);
			lcs_str(tmp,stg[i].name?stg[i].name:"");
		}
		lcs_int("cur_mem",mem);
		lcs_dbl("seek_stride",stride);
		lcs_int("tb_status", ishidden("checkbutton5"));
		lcs_int("tb_zoom", ishidden("checkbutton3"));
		lcs_int("tb_memory", ishidden("checkbutton7"));
		lcs_int("tb_all", ishidden("checkbutton6"));
		lcs_int("tb_transport", ishidden("checkbutton4"));
		lcs_int("tb_unit", ishidden("checkbutton2"));
		lcs_int("tb_timelabels", ishidden("checkbutton8"));
		gint root_x, root_y, width, height;
		gtk_window_get_position (GTK_WINDOW(window1), &root_x, &root_y);
		gtk_window_get_size (GTK_WINDOW(window1), &width, &height);
		lcs_int("win_x", root_x);
		lcs_int("win_y", root_y);
		lcs_int("win_w", width);
		lcs_int("win_h", height);

		for (i=0; i< MAX_KEYBINDING; i++) {
			char tmp[32];
			snprintf(tmp,31,"keybinding_%02i",i+1);
			lcs_int(tmp,key_cfg[i]);
		}

		lcs_int("FR_mode",(jffr==fffr)?1:0);
		lcs_int("FR_flags",uffr->flags);
		//lcs_int("FR_num",uffr->num);
		//lcs_int("FR_den",uffr->den);

		lash_send_event(lash_client, lash_event_new_with_type(LASH_Save_Data_Set));
	} else if (type == LASH_Quit) {
		//loop_flag=0;
		gtk_main_quit();
	} else 
		if (!want_quiet)
			printf ("WARNING: unhandled LASH Event t:%i s:'%s'\n",type,str);
}

void handle_config(lash_config_t* conf) {
	const char*    key      = NULL;
	key      = lash_config_get_key(conf);
	if (!strncmp(key,"memory_",7)) {
		int mymem = atoi(key+7);
		if (mymem < 1) return;
		if (mymem > max_mem) {
			if (realloc_mem(mymem)) return;
		}
		mymem--;
		if (!strcmp(key+9,"_start")) {
			stg[mymem].start=lash_config_get_value_double(conf);
		} else if (!strcmp(key+9,"_end")) {
			stg[mymem].end=lash_config_get_value_double(conf);
		} else if (!strcmp(key+9,"_unit")) {
			stg[mymem].unit=lash_config_get_value_int(conf);
		} else if (!strcmp(key+9,"_name")) {
			if (stg[mymem].name) free(stg[mymem].name);
			stg[mymem].name=strdup(lash_config_get_value_string(conf));
			if (strlen(stg[mymem].name) == 0) {free(stg[mymem].name); stg[mymem].name=NULL;}
			update_combotxt(mymem);
		} else {
			if (!want_quiet)
				printf("Warning: unhandled mem: %s=%f\n",key+7,lash_config_get_value_double(conf));
		}
	} else if (!strncmp(key,"keybinding_",11)) {
		int mykey = atoi(key+11);
		if (mykey < 1 || mykey > MAX_KEYBINDING) return;
		mykey--;
		key_cfg[mykey]=lash_config_get_value_int(conf);
	} else if (!strcmp(key,"syncsource")) {
		dothejack(lash_config_get_value_int(conf));
	} else if (!strcmp(key,"cur_mem")) {
		select_mem(lash_config_get_value_int(conf));
	} else if (!strcmp(key,"seek_stride")) {
		stride=lash_config_get_value_double(conf);
	} else if (!strcmp(key,"tb_status")) {
		showhide("checkbutton5","statusbar1",lash_config_get_value_int(conf));
	} else if (!strcmp(key,"tb_zoom")) {
		showhide("checkbutton3","handlebox3",lash_config_get_value_int(conf));
	} else if (!strcmp(key,"tb_memory")) {
		showhide("checkbutton7","handlebox4",lash_config_get_value_int(conf));
	} else if (!strcmp(key,"tb_all")) {
		showhide("checkbutton6","table1",lash_config_get_value_int(conf));
	} else if (!strcmp(key,"tb_transport")) {
		showhide("checkbutton4","handlebox1",lash_config_get_value_int(conf));
	} else if (!strcmp(key,"tb_unit")) {
		showhide("checkbutton2","handlebox2",lash_config_get_value_int(conf));
	} else if (!strcmp(key,"tb_timelabels")) {
		showhide("checkbutton8","hbox_labels",lash_config_get_value_int(conf));
	} else if (!strcmp(key,"FR_mode")) {
		if (lash_config_get_value_int(conf)) fffr=jffr; // XXX
		else fffr=uffr; 
	} else if (!strcmp(key,"FR_flags")) {
		uffr->flags=lash_config_get_value_int(conf);
	} else if (!strcmp(key,"win_x")) {
		my_root_x=lash_config_get_value_int(conf);
	} else if (!strcmp(key,"win_y")) {
		my_root_y=lash_config_get_value_int(conf);
	} else if (!strcmp(key,"win_w")) {
		my_width=lash_config_get_value_int(conf);
	} else if (!strcmp(key,"win_h")) {
		my_height=lash_config_get_value_int(conf);
	} else {
		unsigned long val_size = lash_config_get_value_size(conf);
		if (!want_quiet)
			printf ("WARNING: unhandled LASH Config.  Key = %s size: %ld\n",key,val_size);
	}
}


/*************************
 *  public LASH functions
 */

void lash_setup(void) {
	lash_event_t *event;
	//lash_config_t *lc;
	event = lash_event_new_with_type(LASH_Client_Name);
	lash_event_set_string(event, "gjacktransport");
	lash_send_event(lash_client, event);
}
#endif


void lash_process(void) {
#ifdef HAVE_LASH
	lash_event_t*  ev = NULL;
	lash_config_t* conf = NULL;
	int flag =0;
	#if 1
		gtk_window_get_size (GTK_WINDOW(window1), &my_width, &my_height);
		gtk_window_get_position (GTK_WINDOW(window1), &my_root_x, &my_root_y);
		const gint prev_w = my_width;
		const gint prev_h = my_height;
		const gint prev_x = my_root_x;
		const gint prev_y = my_root_y;
	#endif
	while ((ev = lash_get_event(lash_client)) != NULL) {
		handle_event(ev);
		lash_event_destroy(ev);
	}
	while ((conf = lash_get_config(lash_client)) != NULL) {
		handle_config(conf);
		lash_config_destroy(conf);
		flag=1;
		// TODO do only if changed! -> flag
	}
	if (flag && no_resize) {
		if (prev_w != my_width || prev_h != my_height) {
			gtk_window_resize(GTK_WINDOW(window1),my_width,my_height);
		}
		if (prev_x != my_root_x || prev_y != my_root_y) {
			gtk_window_set_gravity(GTK_WINDOW(window1), GDK_GRAVITY_STATIC);
			gtk_window_move(GTK_WINDOW(window1),my_root_x,my_root_y);
		}
	}
#endif
}


