#include <gtk/gtk.h>


void
on_spinbutton1_editing_done            (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_spinbutton2_editing_done            (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_hscale1_value_changed               (GtkRange        *range,
                                        gpointer         user_data);

void
on_checkbutton1_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_spinbutton1_value_changed           (GtkSpinButton   *spinbutton,
                                        gpointer         user_data);

void
on_spinbutton2_value_changed           (GtkSpinButton   *spinbutton,
                                        gpointer         user_data);


void
on_hscale1_move_slider                 (GtkRange        *range,
                                        GtkScrollType    scroll,
                                        gpointer         user_data);

gboolean
on_hscale1_button_press_event          (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_hscale1_button_release_event        (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_hscale1_change_value                (GtkRange        *range,
                                        GtkScrollType    scroll,
                                        gdouble          value,
                                        gpointer         user_data);

void
on_button_rewind_released              (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_play_released                (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_pause_released               (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_ff_pressed                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_ff_released                  (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_end_released                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_fr_pressed                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_fr_released                  (GtkButton       *button,
                                        gpointer         user_data);

void
on_button6_released                    (GtkButton       *button,
                                        gpointer         user_data);

void
on_combobox1_changed                   (GtkComboBox     *combobox,
                                        gpointer         user_data);

void
on_combobox2_changed                   (GtkComboBox     *combobox,
                                        gpointer         user_data);

void
on_button7_released                    (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_pos_released                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_combobox3_changed                   (GtkComboBox     *combobox,
                                        gpointer         user_data);

void
on_combobox4_changed                   (GtkComboBox     *combobox,
                                        gpointer         user_data);

void
on_checkbutton4_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_checkbutton2_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_checkbutton5_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_checkbutton3_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_checkbutton6_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_combobox5_changed                   (GtkComboBox     *combobox,
                                        gpointer         user_data);

void
on_checkbutton7_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_checkbutton8_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_button7_released                    (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_smpte_released               (GtkButton       *button,
                                        gpointer         user_data);

void
on_radiobuttonS_jack_toggled           (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_radiobuttonS_vf_toggled             (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

gboolean
on_main_key_press_event                (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

void
on_buttonK0_released                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_buttonK1_released                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_buttonK2_released                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_buttonK3_released                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_buttonK4_released                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_buttonK5_released                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_loadrc_released              (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_saverc_released              (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_saveasrc_released              (GtkButton       *button,
                                        gpointer         user_data);

void
on_button_keyprefs_released            (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_dialog_key_key_press_event          (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

void
on_button8_released                    (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_spinbutton_focus_in_event           (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

gboolean
on_spinbutton_focus_out_event          (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

void
on_buttonK6_released                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_buttonK7_released                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_spinbutton_stride_changed           (GtkEditable     *editable,
                                        gpointer         user_data);
