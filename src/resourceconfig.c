/* gjacktransport - .rc interface
 * Copyright (C) 2007, 2010  Robin Gareus <robin@gareus.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#ifndef GJACKTRANSPORTRC
# define GJACKTRANSPORTRC "gjacktransportrc"
#endif

#ifdef PACKAGE_SYSCONFIGDIR
# define SYSCFGDIR PACKAGE_SYSCONFIGDIR
#else
# define SYSCFGDIR "/etc"
#endif

#ifdef __GNU__
#include <assert.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <gtk/gtk.h>
#include "gjack.h"
#include "gjacktransport.h"
#include "framerate.h"

/* test if file exists and is a regular file - returns 1 if ok */
int testfile (char *filename) {
	struct stat s;
	int result= stat(filename, &s);
	if (result != 0) return 0; /* stat() failed */
	if (S_ISREG(s.st_mode)) return 1; /* is a regular file - ok */
	return(0); 
}

extern int want_quiet;
gint my_root_x, my_root_y, my_width, my_height;
int parse_rc(const char*key, const char *value);
extern GtkWidget *window1;
#define MAX_LINE_LEN 256

void read_rc(const char *fn) {
	FILE* config_fp;
	gtk_window_get_size (GTK_WINDOW(window1), &my_width, &my_height);
	gtk_window_get_position (GTK_WINDOW(window1), &my_root_x, &my_root_y);

	char line[MAX_LINE_LEN];
	int lineno=0;

	if (!want_quiet)
		printf("reading .rc: '%s'\n", fn);

	if (!(config_fp = fopen(fn, "r"))) {
		if (!want_quiet)
			fprintf(stderr,"reading configfile failed: %s (%s)\n",fn,strerror(errno));
		return;
	}
	while( fgets( line, MAX_LINE_LEN-1, config_fp ) != NULL ) {
		char *item, *token, *value;
		lineno++;
		line[MAX_LINE_LEN-1]=0;
		token = strtok(line, "\t =\n\r"); 

		if(token == NULL || token[0] == '#' || token[0] == ';') continue;
		item=strdup(token);
		token = strtok(NULL, "\n\r"); 
		if (!token) {
			free(item);
#ifdef CFG_WARN_ONLY
			printf("WARNING: ignored line in config file. %s:%d\n",fn,lineno);
			continue;
#else
			printf("ERROR parsing config file. %s:%d\n",fn,lineno);
			exit(1);
#endif
		}
		value=strdup(token);
		if (parse_rc(item,value)) {
#ifdef CFG_WARN_ONLY
			printf("WARNING: ignored error in config file. %s:%d\n",fn,lineno);
#else
			printf("ERROR parsing config file. %s:%d\n",fn,lineno);
			exit(1);
#endif
		}
		free(item); free(value);
	}
	fclose(config_fp);

	if (my_width > 0 && my_height > 0)
		gtk_window_resize(GTK_WINDOW(window1),my_width,my_height);
	if (my_root_x > 0 && my_root_y > 0) {
		gtk_window_set_gravity(GTK_WINDOW(window1), GDK_GRAVITY_STATIC);
		gtk_window_move(GTK_WINDOW(window1),my_root_x,my_root_y);
	}
}

void try_load_rc_file (char *filename) {
	if (testfile(filename)) read_rc(filename);
}

void load_rc (void) {
#ifdef __GNU__
	char *home;
	char *filename;
	size_t size, old_size;

	size = strlen(SYSCFGDIR) + strlen(GJACKTRANSPORTRC) + 2;
	filename = malloc(size);
	assert(filename);
	sprintf(filename, "%s/%s", SYSCFGDIR, GJACKTRANSPORTRC);
	try_load_rc_file(filename);

	home = getenv("HOME");
	if (home) {
		old_size = size;
		size = strlen(home) + strlen(GJACKTRANSPORTRC) + 2;
		if (old_size < size) {
			filename = realloc(filename, size);
			assert(filename);
		}
		sprintf(filename, "%s/.%s", home, GJACKTRANSPORTRC);
		try_load_rc_file(filename);
	}

	old_size = size;
	size = strlen(GJACKTRANSPORTRC) + 2;
	if (old_size < size) {
		filename = realloc(filename, size);
		assert(filename);
	}
	sprintf(filename, "./%s", GJACKTRANSPORTRC);
	try_load_rc_file(filename);
	free(filename);
#else
	char *home;
	char filename[PATH_MAX];
	if ((strlen(SYSCFGDIR) + strlen(GJACKTRANSPORTRC) + 2) < PATH_MAX) {
		sprintf(filename, "%s/%s", SYSCFGDIR, GJACKTRANSPORTRC);
		try_load_rc_file(filename);
	}
	home = getenv("HOME");
	if (home && (strlen(home) + strlen(GJACKTRANSPORTRC) + 2) < PATH_MAX) {
		sprintf(filename, "%s/.%s", home, GJACKTRANSPORTRC);
		try_load_rc_file(filename);
	}
	if (strlen(GJACKTRANSPORTRC) + 2 < PATH_MAX) {
		sprintf(filename, "./%s", GJACKTRANSPORTRC);
		try_load_rc_file(filename);
	}
#endif
}

/* vi:set ts=8 sts=2 sw=2: */ 
