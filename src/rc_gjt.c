/* gjacktransport - .rc interface
 * Copyright (C) 2007, 2010  Robin Gareus <robin@gareus.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <gtk/gtk.h>
#include "gjack.h"
#include "gjacktransport.h"
#include "framerate.h"

extern FrameRate *fffr;
extern FrameRate *uffr;
extern FrameRate *jffr;

extern gint key_cfg[MAX_KEYBINDING];

extern int mem;
extern double stride;
extern gjtdata *stg;
extern GtkWidget *window1;
extern int no_lash;
extern int no_resize;
extern char *smpte_font;

extern int want_quiet;
extern int max_mem;
extern gint my_root_x, my_root_y, my_width, my_height;

/* restore config functions - see callbacks.c */
void select_mem (int newmem);
void showhide (char *togglebtn, char *widget, gboolean display);
gboolean ishidden (char *widget); //toggle-btn name
void dothejack (gboolean onoff);
int realloc_mem (int newmax);

int parse_rc(const char*key, const char *value) {
  if (!strncmp(key,"memory_",7)) {
    int mymem = atoi(key+7);
    if (mymem < 1) return -1;
    if (mymem > max_mem) {
      if (realloc_mem(mymem)) return 0;
    }
    mymem--;
    if (!strcmp(key+9,"_start")) {
      stg[mymem].start=strtod(value, NULL);
    } else if (!strcmp(key+9,"_end")) {
      stg[mymem].end=strtod(value, NULL);
    } else if (!strcmp(key+9,"_unit")) {
      stg[mymem].unit=atoi(value);
    } else if (!strcmp(key+9,"_name")) {
      if (stg[mymem].name) free(stg[mymem].name);
      stg[mymem].name=strdup(value);
    } else {
      if (!want_quiet)
	printf("Warning: unhandled mem: %s=%f\n",key+7,strtod(value, NULL));
      return -1;
    }
  } else if (!strncmp(key,"keybinding_",11)) {
    int mykey = atoi(key+11);
    if (mykey < 1 || mykey > MAX_KEYBINDING) return -1;
    mykey--;
    key_cfg[mykey]=atoi(value);
  } else if (!strcmp(key,"syncsource")) {
    dothejack(atoi(value));
  } else if (!strcmp(key,"cur_mem")) {
    select_mem(atoi(value)-1);
  } else if (!strcmp(key,"seek_stride")) {
    stride=strtod(value, NULL);
  } else if (!strcmp(key,"tb_status")) {
    showhide("checkbutton5","statusbar1",atoi(value));
  } else if (!strcmp(key,"tb_zoom")) {
    showhide("checkbutton3","handlebox3",atoi(value));
  } else if (!strcmp(key,"tb_memory")) {
    showhide("checkbutton7","handlebox4",atoi(value));
  } else if (!strcmp(key,"tb_all")) {
    showhide("checkbutton6","table1",atoi(value));
  } else if (!strcmp(key,"tb_transport")) {
    showhide("checkbutton4","handlebox1",atoi(value));
  } else if (!strcmp(key,"tb_unit")) {
    showhide("checkbutton2","handlebox2",atoi(value));
  } else if (!strcmp(key,"tb_timelabels")) {
    showhide("checkbutton8","hbox_labels",atoi(value));
  } else if (!strcmp(key,"FR_mode")) {
    if (atoi(value)) fffr=jffr; // XXX
    else fffr=uffr; 
  } else if (!strcmp(key,"FR_flags")) {
    uffr->flags=atoi(value);
  } else if (!strcmp(key,"win_x")) {
    my_root_x=atoi(value);
  } else if (!strcmp(key,"win_y")) {
    my_root_y=atoi(value);
  } else if (!strcmp(key,"win_w")) {
    my_width=atoi(value);
  } else if (!strcmp(key,"win_h")) {
    my_height=atoi(value);
  } else if (!strcmp(key,"no_lash")) {
    no_lash=atoi(value)?1:0;
  } else if (!strcmp(key,"no_window_resize")) {
    no_resize=atoi(value)?1:0;
  } else if (!strcmp(key,"smpte_font")) {
    if (smpte_font) free(smpte_font);
    smpte_font = strdup(value);
  } else {
    if (!want_quiet)
      fprintf (stderr, "WARNING: unhandled Config parameter  Key = '%s' \n", key);
    return -1;
  }
  return 0;
}

void write_rc(const char *fn) {
  int i;
  FILE* fp;
  if (!(fp = fopen(fn, "w"))) {
    if (!want_quiet)
      fprintf(stderr,"writing configfile failed: %s (%s)\n",fn,strerror(errno));
    return;
  }
  gtk_window_get_size (GTK_WINDOW(window1), &my_width, &my_height);
  gtk_window_get_position (GTK_WINDOW(window1), &my_root_x, &my_root_y);

  fprintf(fp, "# config file for gjacktransport\n#\n# lines beginning with '#' or ';' are ignored.\n#\n");
  fprintf(fp, "# Config files are parsed in the following order, the last read values\n# take precedence.\n#\n");
  fprintf(fp, "#  @sysconfdir@/gjacktransportrc\n#  $HOME/.gjacktransportrc\n#  ./gjacktransportrc\n#\n");
  fprintf(fp, "# -command-line args-\n#  rc-file specified with --config <rc-file>\n#\n");
  fprintf(fp, "# NOTE: @sysconfdir@/gjackclockrc is overwritten by 'make install'\n#\n");

  fprintf(fp, "\n## PRESET/MEMORY ##\n# load-memory preset: 01..99\n");
  fprintf(fp, "cur_mem=%02i\n", mem+1);

  fprintf(fp, "\n# mem-id 01..99\n"
              "# 01..last-found-id presets are created - but at least 6\n"
              "# start/end values are in seconds\n"
              "# unit: hour:0, min:1, sec:2, audio-frames:3 - used for display not config\n"
              "# name: must not be empty but may contain whitespaces\n"
  );
  for (i=0; i< max_mem; i++) {
    if (stg[i].name)
      fprintf(fp, "memory_%02i_name=%s\n", i+1, stg[i].name);
    fprintf(fp, "memory_%02i_start=%f\n", i+1,  stg[i].start);
    fprintf(fp, "memory_%02i_end=%f\n", i+1,  stg[i].end);
    fprintf(fp, "memory_%02i_unit=%i\n\n", i+1, stg[i].unit);
  }

  fprintf(fp, "\n## KEYBINDINGS ##\n");
  const char *kbc[MAX_KEYBINDING] = {
    /* TODO : sync w/ callbacks.c  -> .h*/
    "play/pause", "fast-rew", "fast-ffw", "play", "pause", "rewind", "skip-start", "skip-end" 
  };

  for (i=0; i< MAX_KEYBINDING; i++) {
    fprintf(fp ,"# %s\n", kbc[i]);
    fprintf(fp ,"keybinding_%i=%i\n",i+1, key_cfg[i]);
  }

  fprintf(fp, "\n\n## SHOW/HIDE TOOL-BARS ##\n");
  fprintf(fp, "tb_all=%i\n", ishidden("checkbutton6")?1:0);
  fprintf(fp, "tb_status=%i\n", ishidden("checkbutton5")?1:0);
  fprintf(fp, "tb_zoom=%i\n", ishidden("checkbutton3")?1:0);
  fprintf(fp, "tb_memory=%i\n", ishidden("checkbutton7")?1:0);
  fprintf(fp, "tb_transport=%i\n", ishidden("checkbutton4")?1:0);
  fprintf(fp, "tb_unit=%i\n", ishidden("checkbutton2")?1:0);
  fprintf(fp, "tb_timelabels=%i\n", ishidden("checkbutton8")?1:0);

  fprintf(fp, "\n\n## WINDOW POSITION/SIZE ##\n");
  fprintf(fp, "win_x=%i\n", my_root_x);
  fprintf(fp, "win_y=%i\n", my_root_y);
  fprintf(fp, "win_w=%i\n", my_width);
  fprintf(fp, "win_h=%i\n", my_height);

  fprintf(fp, "\n\n## MISC ##\n");
  fprintf(fp, "# set to 1 to auto-connect to JACK\n");
  fprintf(fp, "syncsource=%i\n", jack_connected()?1:0);
  fprintf(fp, "\n# 1: use JACK video-framerate (if set by time-master)\n");
  fprintf(fp, "FR_mode=%i\n", (jffr==fffr)?1:0);
  fprintf(fp, "\n# seek-stride (in seconds)\n");
  fprintf(fp, "seek_stride=%f\n", stride);
  fprintf(fp, "\n# do not try to use LASH\n");
  fprintf(fp, "no_lash=%i\n", no_lash?1:0);
  fprintf(fp, "\n");
  if (smpte_font) {
    fprintf(fp, "\n# SMPTE display font and optional size\n");
    fprintf(fp, "smpte_font=%s\n", smpte_font);
    fprintf(fp, "\n");
  }

  fclose(fp);
  if (!want_quiet)
    fprintf(stderr,"written config: %s\n",fn);
}

/* vi:set ts=8 sts=2 sw=2: */ 
