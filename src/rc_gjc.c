/* gjackclock - .rc interface
 * Copyright (C) 2010  Robin Gareus <robin@gareus.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include "gjackclock.h"

extern GtkWidget *window1;
extern int want_quiet;
extern gint my_root_x, my_root_y, my_width, my_height;
extern gint key_cfg[MAX_KEYBINDING];
extern char *font_family;
extern int font_size;
extern int want_ontop;
extern int want_ignore_timemaster;
extern int show_fps;
extern int want_scaling;
extern double stride;
extern double user_fps;
extern char *color_bg;
extern char *color_fg;
extern int use_table_layout;

int parse_rc(const char*key, const char *value) {
  my_width=-1; my_height=-1;
  if (!strncmp(key,"keybinding_",11)) {
    int mykey = atoi(key+11);
    if (mykey < 1 || mykey > MAX_KEYBINDING) return -1;
    mykey--;
    key_cfg[mykey]=atoi(value);
  } else if (!strcmp(key,"color_bg")) {
    if (color_bg) free(color_bg);
    color_bg = strdup(value);
  } else if (!strcmp(key,"color_fg")) {
    if (color_bg) free(color_fg);
    color_fg = strdup(value);
  } else if (!strcmp(key,"font_family")) {
    if (font_family) free(font_family);
    font_family = strdup (value);
  } else if (!strcmp(key,"font_size")) {
    font_size = atoi (value);
  } else if (!strcmp(key,"ignore_timebase_master")) {
    want_ignore_timemaster = atoi(value)?1:0;
  } else if (!strcmp(key,"show_fps")) {
    show_fps = atoi(value)?1:0;
  } else if (!strcmp(key,"font_scaling")) {
    want_scaling = atoi(value)?1:0;
  } else if (!strcmp(key,"ontop")) {
    want_ontop = atoi(value)?1:0;
  } else if (!strcmp(key,"table_layout")) {
    use_table_layout = atoi(value)?1:0;
  } else if (!strcmp(key,"fps")) {
    user_fps = atof (value);
  } else if (!strcmp(key,"seek_stride")) {
    stride = atof (value);
    if (stride <=0) stride = 10.0;
  } else if (!strcmp(key,"win_x")) {
    my_root_x=atoi(value);
  } else if (!strcmp(key,"win_y")) {
    my_root_y=atoi(value);
  } else {
    if (!want_quiet)
      fprintf (stderr, "WARNING: unhandled Config parameter  Key = '%s' \n", key);
    return -1;
  }
  return 0;
}

/* vi:set ts=8 sts=2 sw=2: */ 
