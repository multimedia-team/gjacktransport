2016-09-19  x42  <x42@users.sourceforge.net>
  * fix encoding of french translation
  * fix printing to status bar
  * v0.6.3

2016-09-18  x42  <x42@users.sourceforge.net>
  * added French Translations from Olivier Humbert
  * v0.6.2

2013-12-06  x42  <x42@users.sourceforge.net>
  * fix status-bar text-display
	* add option to not auto-shrink window

2012-05-16  x42  <x42@users.sourceforge.net>
  * Add patch to solve FTBFS on the Hurd due to unconditional PATH_MAX
    patch by Cyril Roelandt.
    http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=671586
  * v0.5.3

2010-11-21  x42  <x42@users.sourceforge.net>
  * Mike Cookson's patch to save config on SIGUSR1 - ladish support
  * disabled window maximum size limit for now - not all WMs support that.
  * safeguard for gtk_file_chooser_get_filename()
  * v0.5.2

2010-11-15  x42  <x42@users.sourceforge.net>
  * added load/save/save-as to gjt
  * -l, --config command-line option.
  * cleaned up rc config files.
	* fixed translations & added russion lang.
  * v0.5.1

2010-11-12  x42  <x42@users.sourceforge.net>
  * moved .desktop files to main-src
  * v0.5.0

2010-11-04  x42  <x42@users.sourceforge.net>
  * make table-layout a runtime option
   -> support for variable width fonts.

2010-11-01  x42  <x42@users.sourceforge.net>
  * dedicated icon for gjackclock
  * build-system cleanup
  * added debian packaging 
  * v0.4.4

2010-10-31  x42  <x42@users.sourceforge.net>
  * named memory presets
  * allow to configure SMPTE font in gjt
  * gjt window resize & height fixes.

2010-10-29  x42  <x42@users.sourceforge.net>
  * added big-clock 'gjackclock'
  * honor CTRL-[qw] quit events (both gjackclock & gjacktransport)
  * transport key-bindings for gjackclock
  * added custom-color option for gjackclock
  * always-on-top startup option
  * changed SMPTE text - use dot instead of colon for drop-frame-TC.
  * .rc file for gjackclock

2010-10-26  x42  <x42@users.sourceforge.net>
  * reworked command-line parsing
  * added save-rc function
  * v0.4.0

2010-10-25  x42  <x42@users.sourceforge.net>
  * ignore some modifier keys (shift, ctrl) 
    when setting shortcuts
  * added simple resource-config file (load only)

2010-10-25  x42  <x42@users.sourceforge.net>
  * fixed compiler warning for optional LASH
  * added --disable-lash configure option
  * fixed 'intltoolize'
  * v0.3.1

2010-10-25  x42  <x42@users.sourceforge.net>
  * updated deprecated symbols to recent version
    - jack_client_new()
    - gtk_adjustment_new()
  * enabled support for JackAudioVideoRatio
  * version 0.3.0

2007-06-26  x42  <x42@users.sourceforge.net>
  * added jack_deactivate() call
  * redone keybinding prefs dialog
  * keys '1'-'6' hardcoded to switch memory
  * input-focus on start/stop-spinbtn disables integer-key-events.
  * added fixed skip on key-press
  * workaround sloppy slider update after skip&reposition
  * removed some untranslateables from .pot
  * version 0.2.7

2007-02-23  x42  <x42@users.sourceforge.net>
  * added keybindings to control jack transport
  * version 0.2.6

2007-02-20  x42  <x42@users.sourceforge.net>
  * made JackAudioVideoRatio optional (only for jack >= 0.102 )
  * fixed LASH window position.
  * version 0.2.5

2007-02-19  x42  <x42@users.sourceforge.net>
  * SMPTE + jack framerate
  * fixed transport skip mode

2007-02-05  x42  <x42@users.sourceforge.net>
  * fixed LASH resume (gtk callbacks when lash changes start/end/units)
  * LASH remember start/end lablebar
  * added call jack_activate() -> recv shutdown callback (if any)

2007-02-03  x42  <x42@users.sourceforge.net>
  * updated layout and tooltips
  * slowed transport skip acceleration
  * added start/end lables
  * minimizing height when toggeling toolbars 
  * about-button
  * version v0.2.4

2007-01-30  x42  <x42@users.sourceforge.net>
  * re-wrapped gjacktransport for release v0.2.3
